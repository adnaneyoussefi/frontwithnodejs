import API from '../../config/axios';
export const getmodeles = () => {
    return async (dispatch) => {
        const res = await API.get('modeles');
        dispatch({
            type: "GET_MODELES",
            payload: res.data,
        });
    };
}
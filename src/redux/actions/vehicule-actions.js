import { history } from "../../index";
import API from '../../config/axios';
import { ACTION_FAILED, SEARCH_CARS } from './type';

export const getCars = () => {
    return async (dispatch) => {
        const res = await API.get(`vehicules`);
        dispatch({
            type: "GET_VEHICULES",
            payload: res.data,
        });
    };
}

export const getCar = (vehicule) => {
    return async (dispatch) => {
        dispatch({
            type: "GET_VEHICULE",
            payload: vehicule,
        });
    };
}

export const addCar = (formData, id) => {
    if(id) {
        return dispatch => {
            dispatch(editCar(id, formData));
        }
    }
    else {
        return async dispatch => {
            try {
                const res = await API.post(`vehicules`, formData);
                dispatch({
                    type: "ADD_VEHICULE",
                    payload: res.data,
                });
                history.push("/admin/vehicules");
            } catch(e) {
                dispatch(error(e.response.data.message));
            }
        };
    }
}

export const editCar = (id, formData) => {
    return async dispatch => {
        try {
            await API.put(`vehicules/${id}`, formData);
            history.push("/admin/vehicules");
        }catch(e) {
            console.error(e.response.data);
        }
    };
}

export const deleteCar = (vehicule) => {
    return async (dispatch) => {
        await API.delete(`vehicules/${vehicule._id}`)
        dispatch({
            type: "DELETE_VEHICULE",
            payload: {
                id: vehicule._id
            },
        });
        
    };
}

//CHERCHER PAR DATE-DEBUT ET DATE-FIN (CLIENT)
export const searchCars = (date) => {
    return async (dispatch) => {
        const res = await API.get(`vehicules/chercher/dateDebut/${date.date_debut}/datefin/${date.date_fin}`);
        dispatch({
            type: SEARCH_CARS,
            payload: res.data
        });
        
    };
}

const error = (error) => {
    return {type: ACTION_FAILED, payload: error}
};
import API from '../../config/axios';
import { AUTH_ATTEMPTING, AUTH_SUCCESS, AUTH_FAILED, USER_LOGOUT, PROFILE_FETCHED } from './type';
import setAuthHeader from '../../config/setAuthHeader';

const TOKEN_NAME = 'expense_app_token';

export const signIn = (request_data) => {
    return async dispatch => {
        dispatch({ type: AUTH_ATTEMPTING });
        try {
            const { data } = await API.post('auth', request_data);
            setAuthHeader(data.token);
            dispatch(fetchProfile());
            dispatch(success(data.token));
        }catch(e) {
            dispatch(error(e.response.data.error));  
        }
    }
}

export const onLoadingSignIn = () => {
    return dispatch => {
        try {
            const token = localStorage.getItem(TOKEN_NAME);
            if(token === null || token ==='undefined') {
                return dispatch(error('Vous n\'etes pas connecté!'));
            }
            setAuthHeader(token); 
            dispatch(fetchProfile());   
            dispatch(success(token)); 
        }catch(e) {
            console.error(e)
        }
    }
}

export const fetchProfile = () => {
    return async dispatch => {
        try {
            const { data } = await API.get('me');
            dispatch({ type: PROFILE_FETCHED, payload: data.user });
        }catch(e) {
            console.error(e);
        }
    }
}

export const logUserOut = () => {
    return dispatch => {
        try {
            localStorage.clear();
            dispatch({ type: USER_LOGOUT });
        }catch(e) {
            console.log(e);
        }
    }
}

const success = (token) => {
    localStorage.setItem(TOKEN_NAME, token);
    return {type: AUTH_SUCCESS};
};
const error = (error) => {
    return {type: AUTH_FAILED, payload: error}
};
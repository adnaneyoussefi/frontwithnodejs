import API from '../../config/axios';
import { history } from "../../index";
import { GET_RESERVATIONS, GET_CLIENTRESERVATIONS, LIVRER_RESERVATION } from './type';

//Partie ADMIN
export const getReservations = () => {
    return async dispatch => {
        const res = await API.get(`reservations`);
        dispatch({
            type: GET_RESERVATIONS,
            payload: res.data,
        });
    };
}

export const livrerReservation = (id) => {
    return async dispatch => {
        dispatch({type: 'LOAD_RESERVATION'});
        await API.put(`reservations/${id}`);
        dispatch(getReservations());
        // dispatch({
        //     type: LIVRER_RESERVATION,
        //     payload: id
        // });
    };
}

export const addReservations = (data) => {
    return async dispatch => {
        try {
            await API.post(`reservations`, data);
            history.push('/success');
        }catch(e) {
            console.error(e.response.data);
        }
    };
}

export const getClientReservations = (id) => {
    return async dispatch => {
        try {
            const res = await API.get(`reservations/clients/${id}`);
            dispatch({
                type: GET_CLIENTRESERVATIONS,
                payload: res.data
            })
        }catch(e) {
            console.error(e.response.data);
        }
    };
}


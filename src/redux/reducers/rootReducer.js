import {combineReducers} from "redux";
import auth from './auth-reducer';
import vehiculeReducer from './vehicule-reducer';
import modeleReducer from './modele-reducer';
import reservationReducer from './reservation-reducer';

const rootReducer = combineReducers({
    auth,
    vehicule: vehiculeReducer,
    modele: modeleReducer,
    reservation: reservationReducer
})

export default rootReducer;
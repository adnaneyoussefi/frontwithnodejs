import { GET_RESERVATIONS, GET_CLIENTRESERVATIONS, LIVRER_RESERVATION } from '../actions/type';

const initialState = {
    reservations: [],
    clientReservations: [],
    loading: false
}

const reservationReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_RESERVATIONS:
            return {
                ...state,
                reservations: action.payload, loading: false
            };
        case GET_CLIENTRESERVATIONS:
            return {
                ...state,
                clientReservations: action.payload
            };
        case 'LOAD_RESERVATION':
            return {
                ...state,
                loading: true
            }       
        default:
            return state;
    }
}
export default reservationReducer;
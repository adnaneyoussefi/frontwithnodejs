import { SEARCH_CARS } from '../actions/type';

const initialState = {
    vehicules: [],
    vehiculesCherches: [],
    vehicule: {},
    error: null
}

const vehiculeReducer = (state = initialState, action) => {
    switch(action.type){
        case "GET_VEHICULES":
            return {
                ...state,
                vehicules: action.payload
            };
        case "GET_VEHICULE":
            return {
                ...state,
                vehicule: action.payload
            };
        case "ADD_VEHICULE":
            return {
                ...state,
                vehicules: [...state.vehicules, action.payload]
            };
        case "EDIT_VEHICULE":
            return {
                ...state,
                vehicules: [...state.vehicules, action.payload]
            };            
        case "DELETE_VEHICULE":
            const updatedList = state.vehicules.filter(vehicule => vehicule._id !== action.payload.id)
            return {
                ...state,
                vehicules: updatedList
            };   
        case 'ACTION_FAILED':
            return {
                ...state,
                error: action.payload 
            }; 
        case SEARCH_CARS:
            return {
                ...state,
                vehiculesCherches: action.payload
            };          
        default:
            return state;
    }
}
export default vehiculeReducer;
import { AUTH_ATTEMPTING, AUTH_SUCCESS, AUTH_FAILED, USER_LOGOUT, PROFILE_FETCHED } from '../actions/type';

const initialState = {
    attempting: false,
    isAuth: null,
    profile: {},
    error: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTH_ATTEMPTING:
            return {
                ...state,
                attempting: true, isAuth: false, error: null 
            };
        case AUTH_SUCCESS:
            return {
                ...state,
                attempting: false, isAuth: true, error: false
            };    
        case AUTH_FAILED:
            return {
                ...state,
                attempting: false, isAuth: false, error: action.payload 
            };
        case USER_LOGOUT:
            return {
                ...state,
                isAuth: false, profile: {}, error: null
            }; 
        case PROFILE_FETCHED:
            return {
                ...state,
                profile: action.payload
            };       
        default:
            return state;
    }
}

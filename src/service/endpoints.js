export const CLIENT_ENDPOINT = "/clients";
export const MODELE_ENDPOINT = "/modeles";
//export const MODELE_NO_JSON_ENDPOINT = "/modeles";
export const RESERVATION_ENDPOINT = "/reservations";
export const VEHICULE_ENDPOINT = "/vehicules.json";
export const VEHICULE_NO_JSON_ENDPOINT = "/vehicules";
export const MEDIA_ENDPOINT = "/media_objects.json";
export const MEDIA_NO_JSON_ENDPOINT = "/media_objects";
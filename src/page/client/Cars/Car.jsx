import React from 'react';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import GroupIcon from '@material-ui/icons/Group';
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import SpeedIcon from '@material-ui/icons/Speed';
import ReservForm from '../ReservForm';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import {
    Card, CardImg, CardBody,
    CardTitle, CardSubtitle, Col
  } from 'reactstrap';

const Car = ({ history, vehicule, dates }) => {

    const HtmlTooltip = withStyles((theme) => ({
        tooltip: {
          fontSize: theme.typography.pxToRem(13),
        },
      }))(Tooltip);
      
    return (
            <Col key={vehicule._id}>
            <Card>
                <CardImg height="220px" top src={vehicule.avatar && `http://localhost:5000${vehicule.avatar.replace('public', '')}`} />
                <CardBody>
                    <CardTitle tag="h5" className="font-weight-bold">{vehicule.modele.marque} {vehicule.modele.modele}</CardTitle>
                    <div className="d-flex justify-content-around">
                    {vehicule.climatisation && <HtmlTooltip title="Climatisation disponible">
                        <div className="font-weight-bold"><AcUnitIcon color="primary" fontSize="small" />Clim</div>
                    </HtmlTooltip>}
                    <HtmlTooltip title={`${vehicule.nbrDePersonne} personnes`}>
                        <div className="font-weight-bold"><GroupIcon color="primary" fontSize="small" />{vehicule.nbrDePersonne}</div>
                    </HtmlTooltip>
                    <HtmlTooltip title={`${vehicule.nbrDePorte} portes`}>
                        <div className="font-weight-bold"><DriveEtaIcon color="primary" fontSize="small" />{vehicule.nbrDePorte}</div>
                    </HtmlTooltip>
                    <HtmlTooltip title={`${vehicule.boiteVitesse}`}>
                        <div className="font-weight-bold"><SpeedIcon color="primary" fontSize="small" />{vehicule.boiteVitesse==='Automatique' ? 'A' : 'M'}</div>
                    </HtmlTooltip>
                    </div>
                    <CardTitle tag="h6">{vehicule.carburant}</CardTitle>
                    <CardSubtitle tag="h6" className="mb-2">Prix Pour Jour: <b className="font-weight-bold">{vehicule.prixJournalier} MAD</b></CardSubtitle>
                    <ReservForm dates={dates} buttonLabel="Réserver" vehiculeId={vehicule._id} history={history} status={vehicule.reservation && vehicule.reservation.livré || false}/>
                </CardBody>
            </Card>  
            </Col>
    )
}

export default Car;

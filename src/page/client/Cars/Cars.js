import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    Container, Row
  } from 'reactstrap';
import NavBar from '../../../components/NavBar';
import { getCars, searchCars } from '../../../redux/actions/vehicule-actions';
import Car from './Car';

const Cars = ({ location, history }) => {
    const vehicules = useSelector(state => state.vehicule.vehicules);
    const vehiculesCherches = useSelector(state => state.vehicule.vehiculesCherches);
    const dispatch = useDispatch();

    useEffect(() => {
        if(location.pathname.split('/').slice(-1).join('') === 'chercher') {
            dispatch(searchCars(location.state.dates));
        }
        else {
            dispatch(getCars());
        }        
    }, []);
    return (
        <>
        <NavBar />
        <Container className={"mt-5 pt-5"}>
        <Row xs="3">
        { (location.pathname.split('/').slice(-1).join('') === 'chercher' ? vehiculesCherches : vehicules).map(vehicule =>{
            return (
            <Car history={history} vehicule={vehicule} dates={location.state && location.state.dates || ''}/>
            )
        }) }
        </Row>
        </Container>
        </>
    )
};

export default Cars;

import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, Input, FormGroup, Row,
    Label, Col, FormFeedback, CardTitle } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useSelector, useDispatch } from 'react-redux';
import { addReservations } from '../../redux/actions/reservation-action';
import moment from 'moment';

const ReservForm = ({ buttonLabel, history, vehiculeId, status, dates }) => {
    const user = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const { client } = user.profile;

    return (
        <div>
            <Button color="warning" block onClick={() => user.isAuth ? toggle() : history.push('/login')} disabled={status}>{buttonLabel}</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Informations sur la réservation</ModalHeader>
                <ModalBody>
                    <Formik
                    initialValues={{
                        date_debut: dates.date_debut, date_fin: dates.date_fin, nom: client && client.nom,prenom: client && client.prenom,
                        adresse: client && client.adresse,tel: client && client.tel,ville: client && client.ville
                    }}
                    validationSchema={Yup.object().shape({
                        date_debut: Yup.date().required("Date required"),
                        date_fin: Yup.date().required("Date required"),
                        nom: Yup.string().required('Nom required'),
                        prenom: Yup.string().required('Prenom required'),
                        adresse: Yup.string().required('Adresse required'),
                        tel: Yup.string().required('Telephne required'),
                        ville: Yup.string().required('Ville required'),
                        })  
                    }
                    onSubmit = { values => {
                        values.client = client._id;
                        values.vehiculeId = vehiculeId;
                        dispatch(addReservations(values));
                    }}
                    >
                    {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
                        <Form>
                            <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="date_deb">Date de début</Label>
                                    <Input
                                    invalid={errors.date_debut && touched.date_debut}
                                    type="date"
                                    name="date_debut"
                                    value={values.date_debut}
                                    id="date_deb"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    />
                                    <FormFeedback>{errors.date_debut}</FormFeedback>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>    
                                    <Label for="date_f">Date de fin</Label>
                                    <Input
                                    invalid={errors.date_fin && touched.date_fin}
                                    type="date"
                                    name="date_fin"
                                    value={values.date_fin}
                                    id="date_f"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    />
                                    <FormFeedback>{errors.date_fin}</FormFeedback>
                                </FormGroup>
                            </Col>
                            </Row>
                            <CardTitle>Durée: {moment(values.date_fin).diff(values.date_debut, 'days') + 1} Jours</CardTitle>
                            <FormGroup>
                                <Input 
                                invalid={errors.nom && touched.nom}
                                type="text"
                                name="nom"
                                value={values.nom}
                                placeholder="Nom"
                                onChange={handleChange}
                                onBlur={handleBlur} />
                                <FormFeedback>{errors.nom}</FormFeedback>
                            </FormGroup>
                            <FormGroup>
                                <Input 
                                invalid={errors.prenom && touched.prenom}
                                type="text"
                                name="prenom"
                                value={values.prenom}
                                placeholder="Prenom"
                                onChange={handleChange}
                                onBlur={handleBlur} />
                                <FormFeedback>{errors.prenom}</FormFeedback>
                            </FormGroup>
                            <FormGroup>
                                <Input 
                                invalid={errors.adresse && touched.adresse}
                                type="text"
                                name="adresse"
                                value={values.adresse}
                                placeholder="Adresse"
                                onChange={handleChange}
                                onBlur={handleBlur} />
                                <FormFeedback>{errors.adresse}</FormFeedback>
                            </FormGroup>
                            <FormGroup>
                                <Input 
                                invalid={errors.ville && touched.ville}
                                type="text"
                                name="ville"
                                value={values.ville}
                                placeholder="Ville"
                                onChange={handleChange}
                                onBlur={handleBlur} />
                                <FormFeedback>{errors.ville}</FormFeedback>
                            </FormGroup>
                            <FormGroup>
                                <Input 
                                invalid={errors.tel && touched.tel}
                                type="text"
                                name="tel"
                                value={values.tel}
                                placeholder="Telephone"
                                onChange={handleChange}
                                onBlur={handleBlur} />
                                <FormFeedback>{errors.tel}</FormFeedback>
                            </FormGroup>
                            <ModalFooter>
                            <Button color="primary" onClick={ handleSubmit } disabled={!isValid || isSubmitting}>Valider la réservation</Button>
                            <Button color="secondary" onClick={toggle}>Annuler</Button>
                            </ModalFooter>
                        </Form>
                    )}
                    
                    </Formik>
                </ModalBody>
                
            </Modal>
        </div>
    )
}

export default ReservForm

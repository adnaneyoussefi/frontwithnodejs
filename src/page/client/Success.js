import React from 'react';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import NavBar from '../../components/NavBar';
import { CardTitle, Container } from 'reactstrap';

const Success = () => {
    return (
        <>
            <NavBar />
            <Container className={"d-flex flex-column align-items-center mt-5 pt-5"}>
                <CheckCircleOutlineIcon style={{color: '#04A579', fontSize: 300 }} />
                <CardTitle tag="h5" className="font-weight-bold">Votre réservation est effectuée avec succée</CardTitle>
            </Container>
        </>
    )
}

export default Success

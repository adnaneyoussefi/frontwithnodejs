import React from 'react';
import Carous from '../../components/Carous';
import NavBar from '../../components/NavBar';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Button, Form, Input, FormGroup, Row,
    Label, Col, FormFeedback, Container } from 'reactstrap';
import Footer from './Footer';
const Home = ({ history }) => {
    return (
        <div>
            <NavBar/>
            <Carous/>
            <Container>
                <Row className="m-5">
                    <Col xs="3">
                    <Formik
                    initialValues={{
                        date_debut: '', date_fin: ''
                    }}
                    validationSchema={Yup.object().shape({
                        date_debut: Yup.date().required("Date required"),
                        date_fin: Yup.date().required("Date required")
                        })  
                    }
                    onSubmit = { values => {
                        history.push({
                            pathname: `/vehicules/chercher`,
                            state: {
                                dates: values,
                            }
                        });
                    }}
                    >
                    {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
                    <Form>
                        <FormGroup>
                            <Label for="date_deb">Date de début</Label>
                            <Input
                            invalid={errors.date_debut && touched.date_debut}
                            type="date"
                            name="date_debut"
                            id="date_deb"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            />
                            <FormFeedback>{errors.date_debut}</FormFeedback>
                        </FormGroup>
                        <FormGroup>    
                            <Label for="date_f">Date de fin</Label>
                            <Input
                            invalid={errors.date_fin && touched.date_fin}
                            type="date"
                            name="date_fin"
                            id="date_f"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            />
                            <FormFeedback>{errors.date_fin}</FormFeedback>
                        </FormGroup>
                        <Button color="primary" onClick={ handleSubmit } disabled={!isValid || isSubmitting}>CHERCHER</Button>
                    </Form>)}
                    </Formik>
                </Col>
                </Row>
                </Container>
                <Footer />
        </div>
    )
}

export default Home

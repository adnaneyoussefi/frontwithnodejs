import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getClientReservations } from '../../redux/actions/reservation-action';
import NavBar from '../../components/NavBar';
import { Table, Container, CardImg, Row, Col, CardTitle } from 'reactstrap';
import moment from 'moment';

const Reservations = ({ location }) => {
    const reservations = useSelector(state => state.reservation.clientReservations);
    const dispatch = useDispatch();
    const {client} = location.state;

    useEffect(() => {
        dispatch(getClientReservations(client._id));
    }, []);

    return (
        <>
            <NavBar />
            <Container className={"mt-5 pt-5"}>
                <Table bordered striped dark>
                    <thead>
                        <tr>
                        <th>Description</th>
                        <th>Date de location</th>
                        <th>Prix total</th>
                        </tr>
                    </thead>
                    
                        {reservations.map(reserv => {
                            return(
                                <tbody>
                                <tr>
                                    <td colSpan="3"><b>Numéro de réservation:</b> {reserv._id}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <Row>
                                            <Col>
                                            <CardImg style={{width: '250px'}} top src={`http://localhost:5000${reserv.vehicule.avatar.replace('public', '')}`} />
                                            </Col>
                                            <Col className="d-flex flex-column justify-content-center">
                                            <CardTitle className="font-weight-bold">{reserv.vehicule.modele.marque} {reserv.vehicule.modele.modele}</CardTitle>
                                            <CardTitle>{reserv.vehicule.carburant}</CardTitle>
                                            <CardTitle>Prix du jour: {reserv.vehicule.prixJournalier} MAD</CardTitle>
                                            </Col>
                                        </Row>
                                    </td>
                                    <td>
                                        <CardTitle>Date de début: {moment(reserv.date_debut).format('DD MMMM YYYY')}</CardTitle>
                                        <CardTitle>Date de fin: {moment(reserv.date_fin).format('DD MMMM YYYY')}</CardTitle>
                                        <CardTitle>Durée: {moment(reserv.date_fin).diff(reserv.date_debut, 'days') + 1 } Jours</CardTitle>    
                                    </td>
                                    <td>
                                        <CardTitle>{(moment(reserv.date_fin).diff(reserv.date_debut, 'days') + 1)  * reserv.vehicule.prixJournalier} MAD</CardTitle>
                                    </td>
                                </tr>
                                </tbody>
                            )
                        })}
                        
                </Table>
            </Container>
        </>
    )
}

export default Reservations

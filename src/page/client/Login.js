import React, { useEffect } from 'react';
import { Button, Form, FormFeedback, FormGroup, Input, Alert  } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { signIn } from '../../redux/actions/user-actions';

const Login = (props) => {
    const user = useSelector(state => state.auth);
    const dispatch = useDispatch();

    useEffect(() => {
        if(user.isAuth) {
            props.history.push('/');
        }
    }, [user]);

    return (
    <>
    <div className="App container">
        
        <div className="row login">
            <div className="col" style={{ 
                backgroundImage: `url("../../jant.jpg")`,
                backgroundSize: 'cover' }}>
            </div>
            <div className="col d-flex flex-column justify-content-center">
                <h1>Login</h1>
                {user.error ? <Alert color="danger">
                    {user.error}
                </Alert> : "" }
                
                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                          .email('Invalid email')
                          .required('email required'),
                        password: Yup.string()
                          .min(5, 'Vous devez tapze au moins 5 caractères!')
                          .required('password required')})
                        }
                    onSubmit={ (values, bag) => {
                        dispatch(signIn(values));
                        bag.setSubmitting(false);
                    }}
                    >
                    {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched }) => (
                    <Form onSubmit={ handleSubmit }>
                        <FormGroup>
                            <Input
                                invalid={errors.email && touched.email}
                                type="email" 
                                name="email" 
                                placeholder="E-mail"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                />                              
                            <FormFeedback>{errors.email}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.password && touched.password}
                            type="password" 
                            name="password" 
                            placeholder="Mot de passe"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            />                               
                            <FormFeedback>{errors.password}</FormFeedback>
                        </FormGroup>
                        <Button color="warning" block disabled={!isValid || isSubmitting || user.error == false}>S'identifier</Button>
                    </Form>
                    )}
                </Formik>  
                <span>Vous n'avez pas un compte? <Link to='/signup'>Créer un compte</Link></span>   
            </div>
        </div>
    </div>
    </>
    )
}

export default Login;

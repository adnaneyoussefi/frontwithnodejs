import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Input, FormFeedback, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import API from '../../config/axios';
import { useSelector } from 'react-redux';


const Signup = ({ history }) => {
    const user = useSelector(state => state.auth);
    const [res, setRes] = useState({});
    useEffect(() => {
        if(user.isAuth)
            history.push('/')
    }, [user])
    return (
        <>
        <div className="App">
            <div className="row signup">
                <div className="col align-self-center">
                    <h1>SignUp</h1>
                    {res.error ? <Alert color="danger">
                    {res.error}
                    </Alert> : "" }
                    <Formik
                    initialValues={{
                        nom: '', prenom: '', adresse: '', tel: '', ville: '', email: '',
                        password: ''
                    }}
                    validationSchema={Yup.object().shape({
                        nom: Yup.string().required('Nom required'),
                        prenom: Yup.string().required('Prenom required'),
                        adresse: Yup.string().required('Adresse required'),
                        tel: Yup.string().required('Telephne required'),
                        ville: Yup.string().required('Ville required'),
                        email: Yup.string()
                          .email('Invalid email')
                          .required('email required'),
                        password: Yup.string()
                          .min(5, 'Vous devez tapez au moins 5 caractères!')
                          .required('password required')})
                        }
                    onSubmit={ async (values, bag) => {
                        try {
                            await API.post('register', values);
                            history.push('/login');
                        } catch(e) {
                            setRes(e.response.data);
                        }
                    }}
                    >
                    {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched }) => (
                    <Form onSubmit={ handleSubmit }>
                        <FormGroup>
                            <Input 
                            invalid={errors.nom && touched.nom}
                            type="text"
                            name="nom"
                            placeholder="Nom"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.nom}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.prenom && touched.prenom}
                            type="text"
                            name="prenom"
                            placeholder="Prenom"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.prenom}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.adresse && touched.adresse}
                            type="text"
                            name="adresse"
                            placeholder="Adresse"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.adresse}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.tel && touched.tel}
                            type="text"
                            name="tel"
                            placeholder="Telephone"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.tel}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.ville && touched.ville}
                            type="text"
                            name="ville"
                            placeholder="Ville"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.ville}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.email && touched.email}
                            type="email"
                            name="email"
                            placeholder="E-mail"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.email}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Input 
                            invalid={errors.password && touched.password}
                            type="password"
                            name="password"
                            placeholder="Mot de passe"
                            onChange={handleChange}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.password}</FormFeedback>
                        </FormGroup>
                        <Button color="warning" block disabled={!isValid || isSubmitting}>S'inscrire</Button>
                    </Form>
                    )}
                    </Formik>
                    <span>Déjà inscrit(e)? <Link to='/login'>S'identifier</Link></span>
                </div>
                <div className="col" style={{ 
                backgroundImage: `url("../../audi.jpg")`,
                backgroundSize: 'cover',
                backgroundPosition: 'bottom 0px right -100px' }}></div>
            </div>
        </div>
        </>
    )
}

export default Signup

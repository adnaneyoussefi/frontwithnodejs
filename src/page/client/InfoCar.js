import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, Input, FormGroup, Row,
    Label, Col, FormFeedback, CardTitle } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';

const InfoCar = ({ buttonLabel }) => {
    const user = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <div>
            <Button color="info" block onClick={toggle}>{buttonLabel}</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Informations sur la voiture</ModalHeader>
                <ModalBody>
                    
                </ModalBody>
                
            </Modal>
        </div>
    )
}

export default InfoCar

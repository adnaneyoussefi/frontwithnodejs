import React, { useEffect } from 'react';
import NavBar from '../navbar/NavBar';
import { useSelector, useDispatch } from 'react-redux';
import { getReservations, livrerReservation } from '../../../redux/actions/reservation-action';
import { Table, Container, CardImg, Row, Col, CardTitle } from 'reactstrap';
import moment from 'moment';
import LoadingButton from '@mui/lab/LoadingButton';
import SendIcon from '@mui/icons-material/Send';
import DeleteIcon from '@mui/icons-material/Delete';

const ReservationAdmin = () => {
    const reservations = useSelector(state => state.reservation.reservations);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getReservations());
    }, []);
    
    // Traitement de modifier le status de la livraison
    const loading = useSelector(state => state.reservation.loading);
    function handleChange(id) {
        dispatch(livrerReservation(id));
    }

    return (
        <>
        <Container className={"mt-5"}>
        <NavBar />
            <Table bordered striped dark>
                <thead>
                    <tr>
                    <th>Description</th>
                    <th>Informatons Client</th>
                    <th>Date de location</th>
                    <th>Prix total</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                    {reservations.map(reserv => {
                        return(
                            <tbody>
                            <tr>
                                <td colSpan="6"><b>Numéro de réservation: {reserv._id}</b> {moment(reserv.date_reservation).format('DD/MM/YYYY')}</td>
                            </tr>
                            <tr>
                                <td>
                                    <Row>
                                        <Col>
                                        <CardImg style={{width: '250px'}} top src={`http://localhost:5000${reserv.vehicule.avatar.replace('public', '')}`} />
                                        </Col>
                                        <Col className="d-flex flex-column justify-content-center">
                                        <CardTitle className="font-weight-bold">{reserv.vehicule.modele.marque} {reserv.vehicule.modele.modele}</CardTitle>
                                        <CardTitle>{reserv.vehicule.carburant}</CardTitle>
                                        <CardTitle>Prix du jour: {reserv.vehicule.prixJournalier} MAD</CardTitle>
                                        </Col>
                                    </Row>
                                </td>
                                <td>
                                    <p>Email: {reserv.client.user.email}</p>
                                    <p>Nom complet: {reserv.client.nom} {reserv.client.prenom}</p>
                                    <p>Téléphone: {reserv.client.tel}</p>
                                    <p>Adresse: {reserv.client.adresse}</p>
                                    <p>Ville: {reserv.client.ville}</p>
                                </td>
                                <td>
                                    <CardTitle>Date de début: {moment(reserv.date_debut).format('DD MMMM YYYY')}</CardTitle>
                                    <CardTitle>Date de fin: {moment(reserv.date_fin).format('DD MMMM YYYY')}</CardTitle>
                                    <CardTitle>Durée: {moment(reserv.date_fin).diff(reserv.date_debut, 'days') + 1} Jours</CardTitle>    
                                </td>
                                <td>
                                    <CardTitle>{(moment(reserv.date_fin).diff(reserv.date_debut, 'days') + 1) * reserv.vehicule.prixJournalier} MAD</CardTitle>
                                </td>
                                <td>
                                    <CardTitle>{reserv.livré == 'true' ? 'Livré' : 'Pas encore livré'}</CardTitle>
                                </td>
                                <td>
                                    <LoadingButton
                                        onClick={() => handleChange(reserv._id)}
                                        endIcon={reserv.livré == 'true' ? <DeleteIcon /> : <SendIcon />}
                                        loading={loading}
                                        loadingPosition="end"
                                        variant="contained"
                                    >
                                        {reserv.livré == 'true' ? 'Annuler la livraison' : 'Livrer'}
                                    </LoadingButton>
                                </td>
                            </tr>
                            </tbody>
                        )
                    })}
                    
            </Table>
            </Container>
        </>
    )
}

export default ReservationAdmin;

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

const NavBar = (props) => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <div>
      <Navbar color="faded" light>
        <NavbarBrand className="mr-auto">
          <Link to="/admin/vehicules">Home</Link>
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href="/admin/vehicules">Véhicule</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/admin/modeles">Modèle</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/admin/reservations">Réservation</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/admin/compte">Compte</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;
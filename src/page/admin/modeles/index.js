import React from "react";
import NavBar from "../navbar/NavBar";
import {Card, CardHeader, Table} from "reactstrap";
import { Link } from "react-router-dom";

const Modeles = (props) => {
    return <div className={"container mt-5"}>
        <NavBar />
        <Card>
            <CardHeader>
                La listes des modeles
                    <Link to={"/modeles/add"}>
                    <button className={"btn btn-primary float-right"}>
                        Ajouter un modele
                    </button>
                    </Link>
            </CardHeader>
        </Card>
        <Table>
            
        </Table>
    </div>
}

export default Modeles;
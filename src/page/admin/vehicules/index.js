import React, { useEffect } from 'react';
import { Card, CardHeader, CardBody, CardImg, CardTitle, CardSubtitle, Row, Col, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import NavBar from '../navbar/NavBar';
import { getCars, deleteCar } from '../../../redux/actions/vehicule-actions';
import { useDispatch, useSelector } from 'react-redux';

const Vehicules = ({ history }) => {
    const vehicules = useSelector(state => state.vehicule.vehicules);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCars());
    }, [dispatch])

    const handleDelete = (vehicule) => {
        let signe = window.confirm("Voulez vous supprimer la véhicule?");
        if(!signe) return;
        dispatch(deleteCar(vehicule));
    }

    const showDetails = (vehicule) => {
        history.push({
            pathname: `/admin/vehicules/${vehicule._id}`,
            state: {
                vehicule: vehicule,
            }
        });
    }

    const handleUpdate = (vehicule) => {
        history.push({
            pathname: `/admin/vehicules/update/${vehicule._id}`,
            state: {
                vehicule: vehicule,
            }
        });
    }
    return  (  
    <div className={"container mt-5"}>
        <NavBar />
        <Card>
            <CardHeader>
                La listes des voitures
                <Link to={"/admin/vehicules/add"}>
                <button className={"btn btn-primary float-right"}>
                    Ajouter une vehicule
                </button>
                </Link>
            </CardHeader>
        </Card>
        <Row> 
            {vehicules && vehicules.map(vehicule => {
                return (
                <Col sm="4" key={vehicule._id}>
                <Card body className="text-center">
                    <CardImg height="183px" src={vehicule.avatar && `http://localhost:5000${vehicule.avatar.replace('public', '')}`} alt="Card image cap" />
                    <CardBody>
                        <CardTitle>{vehicule.modele.marque} {vehicule.modele.modele} </CardTitle>
                        <CardSubtitle>Prix journalier : {vehicule.prixJournalier} DH</CardSubtitle>
                        <CardSubtitle>Carburant : {vehicule.carburant}</CardSubtitle>
                        <div className="d-flex justify-content-around">
                        <Button
                            onClick={() => showDetails(vehicule)}
                            className={"btn btn-info"}>
                            Details
                        </Button>
                        <Button
                            onClick={() => handleUpdate(vehicule)}
                            className={"btn btn-success"}>
                            Modifier
                        </Button>
                        <Button
                            onClick={() => handleDelete(vehicule)}
                            className={"btn btn-danger"}>
                            Supprimer
                        </Button>
                        </div>
                    </CardBody>
                </Card>
                </Col>)
                })
            }
        </Row>
    </div>)
}

export default Vehicules;
import React, { useState, useEffect } from 'react';
import { Alert, Button, Form, FormText, Card, CardHeader, CardBody, Input, FormGroup, Label, Col, Row, 
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem, FormFeedback } from "reactstrap";
import { Link } from 'react-router-dom';
import NavBar from "../navbar/NavBar";
import { useDispatch, useSelector} from 'react-redux';
import { getmodeles } from '../../../redux/actions/modele-actions';
import { addCar } from "../../../redux/actions/vehicule-actions";
import { Formik } from 'formik';
import * as Yup from 'yup';

const VehiculeForm =  ({ match, location }) => {
    const error = useSelector(state => state.vehicule.error);
    const dispatch = useDispatch();
    const modeles = useSelector(state => state.modele.modeles);
    const [modele, setModele] = useState();
    const [image_view, setImage_view] = useState('');
    const [dropdownOpen, setDropdownOpen] = useState(false);
    
    const toggle = () => setDropdownOpen(prevState => !prevState);
    let vehicule = {};
    if(match.params.id)
        vehicule = location.state.vehicule;

    const {numSerie= '', matricule= '', kilometrage= '', couleur= '', carburant= '', prixJournalier= '',
    climatisation='', nbrDePersonne= '', nbrDePorte='', boiteVitesse='', avatar= undefined} = vehicule;

    useEffect(() => {
        async function fetchData() {
            dispatch(getmodeles());
            if(!match.params.id) return;
            const src = vehicule.avatar && `http://localhost:5000${vehicule.avatar.replace('public', '')}` || '';
            setImage_view(src);
        }
        fetchData();
    }, [])
    
    const onImageChange = (e, setFieldValue) => {
        setFieldValue('avatar', e.target.files[0])
        let src=URL.createObjectURL(e.target.files[0]);
        setImage_view(src);
    }

    const changeValue = (m) => {
        setModele(m);
    }
    return (
    <div className={"container mt-5"}>  
        <NavBar />
        <Card>
            <CardHeader>Ajouter une voiture :</CardHeader>
            <CardBody>
                {error ? <Alert color="danger">
                    {error}
                </Alert> : "" }
                <Formik
                    initialValues={{
                        numSerie, matricule, kilometrage, couleur, carburant, prixJournalier,
                        climatisation, nbrDePersonne, nbrDePorte, boiteVitesse, avatar
                    }}
                    validationSchema={Yup.object().shape({
                        numSerie: Yup.string().required('Nom required'),
                        matricule: Yup.string().required('Prenom required'),
                        kilometrage: Yup.number().required('Adresse required'),
                        couleur: Yup.string().required('Telephne required'),
                        carburant: Yup.string().required('Ville required'),
                        prixJournalier: Yup.number().required('Ville required'),
                        carburant: Yup.string().required('Ville required'),
                        climatisation: Yup.boolean().required('Climatisation required'),
                        nbrDePersonne: Yup.number().required('Nombre de personne required'),
                        nbrDePorte: Yup.number().required('Nombre de porte required'),
                        boiteVitesse: Yup.string().required('Boite de vitesse required'),
                        avatar: Yup.string().required('Image required'),
                        })
                        }
                    onSubmit={ async (values, bag) => {
                        values.modele = modele && modele._id || 
                        vehicule.modele._id;
                        const formData = new FormData();
                        Object.keys(values).forEach(key => formData.append(key, values[key]));
                        console.log(values)
                        dispatch(addCar(formData, vehicule._id));
                    }}
                    >
                    {({ handleChange, handleSubmit, setFieldValue, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
                    <Form onSubmit={ handleSubmit }>
                        <FormGroup>
                            <Label>Num Serie : </Label>
                            <Input
                                invalid={errors.numSerie && touched.numSerie}
                                name={"numSerie"}
                                type={"text"}
                                placeholder={"Num Serie de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.numSerie}
                            />
                            <FormFeedback>{errors.numSerie}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Matricule : </Label>
                            <Input
                                invalid={errors.matricule && touched.matricule}
                                name={"matricule"}
                                type={"text"}
                                placeholder={"Matricule de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.matricule}
                            />
                            <FormFeedback>{errors.matricule}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Kilometrage : </Label>
                            <Input
                                invalid={errors.kilometrage && touched.kilometrage}
                                name={"kilometrage"}
                                type={"text"}
                                placeholder={"Kilometrage de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.kilometrage}
                            />
                            <FormFeedback>{errors.kilometrage}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Couleur : </Label>
                            <Input
                                invalid={errors.couleur && touched.couleur}
                                name={"couleur"}
                                type={"text"}
                                placeholder={"Couleur de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.couleur}
                            />
                            <FormFeedback>{errors.couleur}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Carburant : </Label>
                            <Input
                                invalid={errors.carburant && touched.carburant}
                                name={"carburant"}
                                type={"text"}
                                placeholder={"Carburant de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.carburant}
                            />
                            <FormFeedback>{errors.carburant}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Prix journalier : </Label>
                            <Input
                                invalid={errors.prixJournalier && touched.prixJournalier}
                                name={"prixJournalier"}
                                type={"text"}
                                placeholder={"Prix journalier de vehicule"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.prixJournalier}
                            />
                            <FormFeedback>{errors.prixJournalier}</FormFeedback>
                        </FormGroup>
                        <FormGroup tag="fieldset">
                            <Label>Climatisation:</Label>
                            <FormGroup check>
                                <Label check>
                                    <Input
                                    type="radio" name="climatisation" value={true}
                                    onChange={handleChange}
                                    onBlur={handleBlur}/>{' '}
                                    Oui
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input
                                    type="radio" name="climatisation" value={false} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}/>{' '}
                                    Non
                                </Label>
                            </FormGroup>
                        </FormGroup>
                        <FormGroup>
                            <Label>Nombre de personne : </Label>
                            <Input
                                invalid={errors.nbrDePersonne && touched.nbrDePersonne}
                                name={"nbrDePersonne"}
                                type={"text"}
                                placeholder={"Nombre de personne dans la voiture"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.nbrDePersonne}
                            />
                            <FormFeedback>{errors.nbrDePersonne}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Nombre de porte : </Label>
                            <Input
                                invalid={errors.nbrDePorte && touched.nbrDePorte}
                                name={"nbrDePorte"}
                                type={"text"}
                                placeholder={"Nombre de porte dans la voiture"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.nbrDePorte}
                            />
                            <FormFeedback>{errors.nbrDePorte}</FormFeedback>
                        </FormGroup>
                        <FormGroup tag="fieldset">
                            <Label>Boite de vitesse:</Label>
                            <FormGroup check>
                                <Label check>
                                    <Input
                                    type="radio" name="boiteVitesse" value={"Automatique"}
                                    onChange={handleChange}
                                    onBlur={handleBlur}/>{' '}
                                    Automatique
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input
                                    type="radio" name="boiteVitesse" value={"Manuelle"}
                                    onChange={handleChange}
                                    onBlur={handleBlur}/>{' '}
                                    Manuelle
                                </Label>
                            </FormGroup>
                        </FormGroup>
                        <FormGroup>
                            <Label for="avatarid">Image</Label>
                            <Input type="file" name="avatar" id="avatarid" 
                            onChange={(e) => onImageChange(e, setFieldValue)}
                            onBlur={handleBlur} />
                            <FormFeedback>{errors.avatar}</FormFeedback>
                            <FormText color="muted">
                            Choissez l'image du véhicule.
                            </FormText>
                            <img src={image_view} alt="" width="308px" height="183px"></img>
                        </FormGroup>
                        <FormGroup>
                            <Label>Modele : </Label>
                            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                                <DropdownToggle caret>
                                {modele && modele.marque || vehicule.modele && vehicule.modele.marque || "Selectionner un modèle"} {modele && modele.modele || vehicule.modele && vehicule.modele.modele}
                                </DropdownToggle>
                                <DropdownMenu>
                                    {modeles.map(modele => 
                                        <DropdownItem onClick={() => changeValue(modele)} key={modele._id}> {modele.marque} {modele.modele}</DropdownItem>  
                                        )}                                  
                                </DropdownMenu>
                            </Dropdown>
                        </FormGroup>
                        <Row>
                            <Col className={"offset-10"}>
                                <Button color="warning" block disabled={!isValid}>{match.params.id ? "Modifier" : "Ajouter"}</Button>
                            </Col>
                        </Row>
                    </Form>
                    )}
                    </Formik>
                <Link to={"/admin/vehicules"}>
                    <Button className={"btn btn-info"}>Retour à la liste</Button>
                </Link>
            </CardBody>
        </Card>
    </div>);
}

export default VehiculeForm;
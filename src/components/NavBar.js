import React, { useState } from 'react';
import { history } from "../index";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';  
import { logUserOut } from '../redux/actions/user-actions';

const NavBar = () => {
  const user = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);  

  const {client} = user.profile;
  
  const renderLoginOrLogout = () => {
    if(user.isAuth)
      return(
        <Nav navbar>
          <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
              Bonjour {client && client.nom} {client && client.prenom}
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                COMPTE
              </DropdownItem>
              <DropdownItem 
              onClick={() => history.push({
                pathname: `/reservations`,
                state: {
                  client
                }
              })}
              >
                MES RESERVATIONS
              </DropdownItem>
              <DropdownItem onClick={() => dispatch(logUserOut())}>
                DECONNEXION
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      )
    else  
      return (
        <Nav navbar>
          <NavItem>
            <NavLink href="/login/">Login</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/signup/">Register</NavLink>
          </NavItem>
        </Nav>
      )
  }

  return (
    <div>
      <Navbar scrolling color="dark" dark expand="md" fixed="top">
        <NavbarBrand href="/">Home</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/vehicules/">NOS VEHICULES</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/components/">NOS PROMOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">NOUS CONTACTER</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                OPTIONS
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Option 1
                </DropdownItem>
                <DropdownItem>
                  Option 2
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Reset
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          {renderLoginOrLogout()}
            
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;
import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

const items = [
  {
    src: '../../volswagen1.jpg',
    altText: 'Volswagen',
    caption: 'Volswagen',
  },
  {
    src: '../../mercedes1.jpg',
    altText: 'Mercedes',
    caption: 'Mercedes'
  },
  {
    src: '../../audi2.jpg',
    altText: 'Audi',
    caption: 'Audi'
  },
  {
    src: '../../renault.jpg',
    altText: 'Renault',
    caption: 'Renault'
  },
  {
    src: '../../citroen.jpg',
    altText: 'Citroën',
    caption: 'Citroën'
  },
  {
    src: '../../peugeot.jpg',
    altText: 'Peugeot',
    caption: 'Peugeot'
  }
];

const Carous = () => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} style={{ objectFit: 'cover' }} width="100%" height="660vh"/>
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default Carous;
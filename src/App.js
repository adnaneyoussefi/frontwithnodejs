import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import VehiculeForm from "./page/admin/vehicules/form";
import Vehicules from './page/admin/vehicules';
import Vehicule from './page/admin/vehicules/show';
import Modeles from './page/admin/modeles';
import { Provider } from "react-redux";
import store from "./redux/store";
import Login from './page/client/Login';
import Signup from './page/client/Signup';
import Home from './page/client/Home';
import { onLoadingSignIn } from './redux/actions/user-actions';
import ProtectedRoutes from './components/ProtectedRoutes';
import Cars from './page/client/Cars/Cars';
import Success from './page/client/Success';
import Reservations from './page/client/Reservations';
import ReservationAdmin from './page/admin/reservations';

function App() {
  store.dispatch(onLoadingSignIn());
  return (
    <Provider store = { store }>
      <BrowserRouter>
        <Switch>
          <Route path={"/"} component={Home} exact/>
          <Route path={"/login"} component={Login} exact/>
          <Route path={"/signup"} component={Signup} exact/>
          <Route path={"/vehicules"} component={Cars} exact/>
          <Route path={"/vehicules/chercher"} component={Cars} exact/>
          <ProtectedRoutes path={"/reservations"} component={Reservations} exact/>
          <ProtectedRoutes path={"/success"} component={Success} exact/>
          <ProtectedRoutes path={"/admin/vehicules"} component={Vehicules} exact/>
          <ProtectedRoutes path={"/admin/vehicules/add"} component={VehiculeForm} exact/>
          <ProtectedRoutes path={"/admin/vehicules/:id"} component={Vehicule} exact/>
          <ProtectedRoutes path={"/admin/vehicules/update/:id"} component={VehiculeForm} exact/>
          <ProtectedRoutes path={"/admin/modeles"} component={Modeles} exact/> 
          <ProtectedRoutes path={"/admin/reservations"} component={ReservationAdmin} exact/>              
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;